package clean.rest.controller;

import org.springframework.web.bind.annotation.*;

/**
 *
 * @author thomas
 */
@RestController
@RequestMapping("system")
public class SystemController {

    @GetMapping("hello")
    public String getTestMessage() {
        return "Hello, world!";
    }
}
